import React from 'react';

import SuccessMessage from './SuccessMessage';
import { checkErrorOnSubmit, getErrorByValidateData } from './validateData';

class Form extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            age: '',
            gender: '',
            role: '',
            email: '',
            password: '',
            repeatPassword: '',
            tos: false,
            errors: {},
            AllFieldsDone: false,
        };
    }

    handleInput = (event) => {
        const {
            name,
            value
        } = event.target;

        this.setState({
            [name]: value
        });

        let errorMessage;

        if (value === "") {
            errorMessage = `${name} cannot be left blank`;
        } else {
            errorMessage = getErrorByValidateData(name, value, this.state.password);
        }


        this.setState((prevState) => {
            const newErrors = {
                ...prevState.errors,
                [name]: errorMessage
            }

            return {
                ...prevState,
                errors: newErrors
            }
        });
    }

    handleCheckbox = (event) => {
        this.setState({
            tos: event.target.checked
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        console.log('handleSubmit');

        if (Object.keys(this.state.errors).length !== 0) {
            const errors = checkErrorOnSubmit(this.state.errors);
            if (Object.keys(errors).length === 0) {
                this.setState({
                    AllFieldsDone: true,
                });
            } else {
                return;
            }
        } else {
            return;
        }
    }

    render() {
        const {
            firstName,
            lastName,
            age,
            gender,
            role,
            email,
            password,
            repeatPassword,
            tos,
            errors
        } = this.state;

        return (
            <>
                {!this.state.AllFieldsDone &&
                    <div className="col-md-10 col-lg-6 col-xl-6">
                        <div className="card shadow-lg">
                            <div className="card-body p-3">
                                <h3 className="text-center m-0 text-success">Sign Up Form</h3>
                                <form onSubmit={this.handleSubmit} className="row mx-5 p-3">
                                    <div className="mb-4 g-0 form-group">
                                        <span>
                                            <i className="fa fa-user me-2"></i>
                                            <label className='form-label mb-0 mt-1' htmlFor='firstNameID'> First name</label>
                                        </span>
                                        <input
                                            type="text"
                                            className='form-control'
                                            id='firstNameID'
                                            name="firstName"
                                            value={firstName}
                                            onChange={this.handleInput}
                                            required
                                        />
                                        <small className='position-absolute text-danger'>{errors.firstName}</small>
                                    </div>

                                    <div className="mb-4 g-0 form-group">
                                        <span><i className="fa fa-user me-2"></i>
                                            <label className='form-label mb-0 mt-1' htmlFor='lastNameID'> Last name</label></span>
                                        <input
                                            type="text"
                                            id='lastNameID'
                                            className='form-control'
                                            name="lastName"
                                            value={lastName}
                                            onChange={this.handleInput}
                                            required
                                        />
                                        <small className='position-absolute text-danger'>{errors.lastName}</small>
                                    </div>
                                    <div className="mb-4 col-md-12 col-12 p-0 mt-2 form-group">
                                        <label className='form-label mb-0' htmlFor='ageID'>Age</label>
                                        <input
                                            type="text"
                                            id='ageID'
                                            className='form-control'
                                            name="age"
                                            value={age}
                                            onChange={this.handleInput}
                                            required
                                        />
                                        <small className='position-absolute text-danger'>{errors.age}</small>
                                    </div>

                                    <div className='mb-4 scroll-down-container col-md-6 col-lg-6 row-sm-12 p-0 mt-2'>
                                        <div className="form-group">
                                            <span>
                                                <i className="fa fa-venus-mars"></i>
                                                <label className='form-label mb-0'>Gender</label>
                                            </span>
                                            <select className='form-control' name="gender" value={gender} onChange={this.handleInput} required>
                                                <option value="">Select</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                                <option value="Other">Other</option>
                                            </select>
                                            <small className='position-absolute text-danger'>{errors.gender}</small>
                                        </div>
                                    </div>

                                    <div className='mb-4 scroll-down-container col-md-6 col-lg-6 row-sm-12 p-0 mt-2'>
                                        <div className="form-group"><label className='form-label mb-0' htmlFor=''>Role</label>
                                            <select className='form-control' name="role" value={role} onChange={this.handleInput} required >
                                                <option value="">Select</option>
                                                <option value="developer">Developer</option>
                                                <option value="seniorDeveloper">Senior Developer</option>
                                                <option value="leadEngineer">Lead Engineer</option>
                                                <option value="CTO">CTO</option>
                                            </select>
                                            <small className='position-absolute text-danger'>{errors.role}</small>
                                        </div>
                                    </div>

                                    <div className="mb-4 g-0 mt-2 form-group">
                                        <span><i className="fa fa-envelope me-2"></i>
                                            <label className='form-label mb-0 mt-1' htmlFor='emailID'>
                                                Email</label></span>
                                        <input
                                            type="email"
                                            className='form-control'
                                            id='emailID'
                                            name="email"
                                            value={email}
                                            onChange={this.handleInput}
                                            required
                                        />
                                        <small className='position-absolute text-danger'>{errors.email}</small>
                                    </div>

                                    <div className="mb-5 g-0 form-group">
                                        <span>
                                            <i className="fa fa-lock me-2"></i>
                                            <label className='form-label mb-0 mt-1' htmlFor='passwordID'>
                                                Password</label>
                                        </span>
                                        <input
                                            type="password"
                                            className='form-control'
                                            id='passwordID'
                                            name="password"
                                            value={password}
                                            onChange={this.handleInput}
                                            required
                                        />
                                        <small className='position-absolute text-danger'>{errors.password}</small>
                                    </div>

                                    <div className="mt-1 mb-4 g-0 form-group">
                                        <span><i className="fa fa-key me-2"></i>
                                            <label className='form-label mb-0 mt-1' htmlFor='repeatPasswordID'>Repeat Password</label></span>
                                        <input
                                            type="password"
                                            id='repeatPasswordID'
                                            className='form-control'
                                            name="repeatPassword"
                                            value={repeatPassword}
                                            onChange={this.handleInput}
                                            required
                                        />
                                        <small className='position-absolute text-danger'>{errors.repeatPassword}</small>
                                    </div>

                                    <div className="mb-4 form-check d-flex justify-content-center g-2">
                                        <input
                                            type="checkbox"
                                            className='form-check-input me-2'
                                            id='tosID'
                                            name="tos"
                                            checked={tos}
                                            onChange={this.handleCheckbox}
                                            required
                                        />
                                        <label className='form-check-label' htmlFor="tosID"> I agree with all <a href="/">Terms and Conditions</a></label>
                                    </div>
                                    <button type="submit" className='btn btn-success mt-2'>Sign Up</button>
                                </form>
                            </div>
                        </div>
                    </div>
                }

                {this.state.AllFieldsDone && <SuccessMessage />}
            </>
        );
    }
}


export default Form;