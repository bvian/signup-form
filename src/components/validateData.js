import validator from 'validator';

export function getErrorByValidateData(name, value, password) {
  let error = '';

  switch (name) {
    case 'firstName':
      error = !validator.isAlpha(value) ? 'Please enter name using letters only' : '';
      break;

    case 'lastName':
      error = !validator.isAlpha(value) ? 'Please enter name using letters only' : '';
      break;

    case 'age':
      error = !validator.isInt(value)? 'age must be a number' : (value < 18 || value > 60) ? 'Age must be between 18 to 60' : '';
      break;

    case 'gender':
      error = !value ? 'Choose one' : '';
      break;

    case 'role':
      error = !value ? 'Role is required' : '';
      break;

    case 'email':
      error = !validator.isEmail(value) ? 'Please enter a valid email address' : '';
      break;

    case 'password':
      error = !validator.isStrongPassword(value) ? 'Password must contain: minimum of 8 characters long, one numeric, one lowercase, one Uppercase, one character.' : '';
      break;

    case 'repeatPassword':
      error = (password !== value || value === '') ? 'Password must be same' : '';
      break;

    case 'tos':
      error = !value ? 'Please agree to our terms and conditions' : '';
      break;

    default:
      break;
  }

  return error;
}

export function checkErrorOnSubmit(errors) {
  const allErrors = Object.entries(errors)
    .reduce((errorsData, error) => {
      if (error[1] === "") {
        return errorsData;
      } else {
        errorsData[error[1]] = error[2];
        return errorsData;
      }

    }, {});

  return allErrors;
}
